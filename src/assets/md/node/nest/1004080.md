### config

```js
https://www.npmjs.com/package/@nestjs/config

基于dotenv的@nestjs/config包来更灵活地进行项目配置
https://zhuanlan.zhihu.com/p/123338172
```

#### 安装

```js
npm i --save @nestjs/config
```

#### 新增配置文件

```js
https://juejin.cn/post/7222949516021727289
安装完成后，我们在和package.json同级别的目录上新建.env文件

DB_TYPE=MYSQL
DB_NAME=mydemo
DB_URL=http://localhost:3306
DB_USER=root
DB_PWD=root
```

#### 配置导入

```js
同时我们修改app.module.ts文件，引入ConfigModule
并将isGlobal设置为true，让ConfigModule可以全局使用
```

```js
import { Module } from "@nestjs/common";
import { AppController } from "./app.controller";
import { AppService } from "./app.service";
import { ConfigModule } from "@nestjs/config";

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
```

```js
接下来我们修改app.controller.ts,并在构造函数中引入ConfigService
我们新增一个 API，在 API 里面解析出.env里面的参数并将参数返回。
```

```js
import { Controller, Get } from "@nestjs/common";
import { AppService } from "./app.service";
import { ConfigService } from "@nestjs/config";

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly configService: ConfigService
  ) {}

  @Get("config")
  getConfig(): any {
    const config = {
      dbType: this.configService.get("DB_TYPE"),
      dbName: this.configService.get("DB_NAME"),
      dbUrl: this.configService.get("DB_URL"),
      dbUser: this.configService.get("DB_USER"),
      dbPwd: this.configService.get("DB_PWD"),
    };
    return config;
  }
}
```

#### 开发环境/正式环境

```js
先在.env文件同级目录下新建两个文件，拷贝.env内容到两个新文件

.env.development
.env.production
```

```js
修改三个文件中的DB_NAME的值，分别添加后缀 -dev -prod
DB_NAME=mydemo-dev
DB_NAME=mydemo-prod
```
##### 在启动命令中设置环境变量
```js
需要增加corss-env插件
我们就可以在启动命令行上增加环境变量传递给应用程序
npm install --save-dev cross-env
```
```js
修改package.json
在启动命令上添加环境变量 cross-env NODE_ENV 参数
"start:dev": "cross-env NODE_ENV=development nest start --watch",
"start:debug": "nest start --debug --watch",
"start:prod": "cross-env NODE_ENV=production node dist/main",
```

##### 配置文件读取

```js
修改app.module.ts，引入环境变量
并在 ConfigModule 参数里面引入环境变量
```

```js
import { Module } from "@nestjs/common";
import { AppController } from "./app.controller";
import { AppService } from "./app.service";
import { ConfigModule } from "@nestjs/config";

const envFilePath = `.env.${process.env.NODE_ENV || `development`}`;

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: envFilePath,
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
```

```js
envFilePath:'.env', 
//配置文件路径，也可以配置为数组如['/config/.env1','.env']。
ignoreEnvFile:false, 
//忽略配置文件，为true则仅读取操作系统环境变量，常用于生产环境
isGlobal:true,       
//配置为全局可见，否则需要在每个模块中单独导入ConfigModule
load:[configuration,databaseconfig],
//导入自定义配置文件，见下节
```

##### 抽离公共配置项

```js
利用之前的.env配置文件，
只在.env配置文件中配置DB_TYPE=MYSQL，
其余配置文件删除DB_TYPE=MYSQL配置项
```

```js
先安装插件dotenv
在app.module.ts中修改 ConfigModule 参数，给load赋值，指定读取.env配置文件
import { Module } from "@nestjs/common";
import { AppController } from "./app.controller";
import { AppService } from "./app.service";
import { ConfigModule } from "@nestjs/config";
import * as dotenv from "dotenv";

const envFilePath = `.env.${process.env.NODE_ENV || `development`}`;

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: envFilePath,
      load: [() => dotenv.config({ path: ".env" })],
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
```

#### 自定义配置文件

```js
config/configuration.ts

export default () => ({
  database: {
    host: process.env.DATABASE_HOST,
    port: parseInt(process.env.DATABASE_PORT, 10) || 5432
  }
});
```

```js
app.controller.ts

import {ConfigService} from '@nestjs/config';
export class AppController {
  constructor(
    private readonly appService: AppService,
    private  configService:ConfigService
    ) {}

    getDataHost():string{
        const DataHost:string=this.configService.get<string>('database.host');
        return DataHost;
    }
 }
```





