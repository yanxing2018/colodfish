### typeorm

```js
https://typeorm.io/
```

#### 安装

````js
npm install --save @nestjs/typeorm typeorm mysql2
````

#### 配置

```js
.env

DATABASE_HOST=localhost
DATABASE_PORT=3306
DATABASE_USER=root
DATABASE_PASS=123456
DATABASE_NAME=coldfish01
```

```js
src\config\typeOrm.config.ts

import { ConfigModule, ConfigService } from "@nestjs/config"
import { TypeOrmModuleAsyncOptions } from "@nestjs/typeorm"


export const typeOrmConfig:TypeOrmModuleAsyncOptions ={
    imports: [ConfigModule],  
    inject: [ConfigService],    
    useFactory: (configService: ConfigService) => ({
      type: 'mysql',
      host: configService.get<string>('DATABASE_HOST'),
      port: parseInt(configService.get<string>('DATABASE_PORT')),
      username: configService.get<string>('DATABASE_USER'),
      password: configService.get<string>('DATABASE_PASS'),
      database: configService.get<string>('DATABASE_NAME'),
      entities: [__dirname + '/**/*.entity{.ts,.js}'],
      synchronize: true, // 同步，确保我们的TypeORM实体在每次运行应用程序时都会与数据库同步 生产环境设置为true，开发环境设置为false
      autoLoadEntities: true, // 自动载入模型到数据库,推荐使用 typerom migration 方案
    })
  }

?
autoLoadEntities: true, // 自动链接被 forFeature 注册的实体
synchronize: true, // 实体与表同步 调试模式下开始。不然会有强替换导致数据丢是
```

```js
src\app.module.ts

import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeOrmConfig } from './config/typeOrm.config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true}),      // 配置文件
    TypeOrmModule.forRootAsync(typeOrmConfig),      // 数据库
    UserModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
```



