import { createFromIconfontCN } from "@ant-design/icons";

const Iconfont = createFromIconfontCN({
  scriptUrl: "//at.alicdn.com/t/c/font_3202486_iczj8nm3zn.js", // 在 iconfont.cn 上生成
})

export default Iconfont;
