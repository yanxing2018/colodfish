import ReactDOM from "react-dom";
import App from "./App";
import { ConfigProvider } from "antd";
import locale from "antd/lib/locale/zh_CN";
import moment from "moment";
import "moment/locale/zh-cn";
import { HashRouter } from "react-router-dom";
import { Provider } from "react-redux";
import store from "store";
import './mock/index'

moment.locale("zh-cn");
ReactDOM.render(
  <ConfigProvider locale={locale}>
    {/* react-router */}
    <HashRouter>
      {/* redux */}
      <Provider store={store}>
        <App />
      </Provider>
    </HashRouter>
  </ConfigProvider>,
  document.getElementById("root")
);
