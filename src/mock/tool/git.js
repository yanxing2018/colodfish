import Mock from 'mockjs'
export default Mock.mock('/tool/git', 'get', options => {
    return Mock.mock([
        {
            key: 801000,
            title: "安装使用",
        },
        {
            key: 801001,
            title: "git命令",
        },
        {
            key: 801002,
            title: "四个区",
        },
        {
            key: 801011,
            title: "删除分支",
        },
        {
            key: 801020,
            title: "删除大文件",
        },
        {
            key: 801021,
            title: "github",
        },
        {
            key: 801022,
            title: "github加速",
        },
        {
            key: 801023,
            title: "GitHub Copilot",
        },
        {
            key: 801030,
            title: "gitee发布静态",
        },
    ]);
})