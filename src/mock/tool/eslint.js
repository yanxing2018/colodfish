import Mock from 'mockjs'
export default Mock.mock('/tool/eslint', 'get', options=>{
    return Mock.mock([
        {
            key: 808000,
            title:"介绍",
        },
        {
            key: 808001,
            title:"react配置",
        },
        {
            key: 808011,
            title:"阻止不规范提交",
        },
    ]);
})