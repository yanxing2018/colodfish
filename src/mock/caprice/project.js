import Mock from 'mockjs'
export default Mock.mock('/caprice/project', 'get', options => {
    return Mock.mock([
        {
            key: 101000,
            title: "项目目标",
        },
        {
            key: 101001,
            title: "项目构成",
        },
        {
            key: 101002,
            title: "项目更新",
        },
    ]);
})