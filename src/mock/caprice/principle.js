import Mock from 'mockjs'
export default Mock.mock('/caprice/principle', 'get', options => {
    return Mock.mock([
        {
            key: 102000,
            title: "正态分部",
        },
        {
            key: 102001,
            title: "黄金分割",
        },
        {
            key: 102002,
            title: "三大八项",
        },
        {
            key: 102003,
            title: "论师长",
        },
        {
            key: 102004,
            title: "赵充国传",
        },
    ]);
})