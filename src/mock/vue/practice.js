import Mock from 'mockjs'
export default Mock.mock('/vue/practice', 'get', options=>{
    return Mock.mock([
        {
            key: 508000,
            title:"项目初始化",
          },
          {
            key: 508001,
            title:"路径别名",
          },
          {
            key: 508002,
            title:"eslint",
          },
          {
            key: 508003,
            title:"重置样式",
          },
    ]);
})