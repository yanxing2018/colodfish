import Mock from 'mockjs'
export default Mock.mock('/node/express', 'get', options => {
  return Mock.mock([
    {
      key: 1002000,
      title: "介绍",
    },
    {
      key: 1002001,
      title: "基本使用",
    },
    {
      key: 1002002,
      title: "请求参数",
    },
    {
      key: 1002010,
      title: "托管静态资源",
    },
    {
      key: 1002020,
      title: "nodemon",
    },
    {
      key: 1002030,
      title: "路由",
    },
    {
      key: 1002040,
      title: "中间件",
    },
    {
      key: 1002041,
      title: "中间件分类",
    },
    {
      key: 1002042,
      title: "自定义中间件",
    },
    {
      key: 1002050,
      title: "写接口",
    },
    {
      key: 1002051,
      title: "跨域问题",
    },
    {
      key: 1002060,
      title: "mysql模块",
    },
    {
      key: 1002061,
      title: "操作数据",
    },
    {
      key: 1002062,
      title: "session",
    },
    {
      key: 1002063,
      title: "jwt",
    },
  ])
})