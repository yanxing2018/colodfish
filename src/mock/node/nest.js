import Mock from "mockjs";
export default Mock.mock("/node/nest", "get", (options) => {
  return Mock.mock([
    {
      key: 1004000,
      title: "介绍",
    },
    {
      key: 1004001,
      title: "项目",
    },
    {
      key: 1004010,
      title: "nestjs/cli",
    },
    {
      key: 1004011,
      title: "热启动",
    },
    {
      key: 1004020,
      title: "处理请求",
    },
    {
      key: 1004021,
      title: "装饰器",
    },
    {
      key: 1004022,
      title: "参数限制与转换",
    },
    {
      key: 1004023,
      title: "自定义装饰器",
    },
    {
      key: 1004030,
      title: "middleware",
    },
    {
      key: 1004032,
      title: "exception",
    },
    {
      key: 1004034,
      title: "pipe",
    },
    {
      key: 1004036,
      title: "guard",
    },
    {
      key: 1004040,
      title: "日志",
    },
    {
      key: 1004050,
      title: "数据库连接",
    },
    {
      key: 1004051,
      title: "typeorm",
    },
    {
      key: 1004052,
      title: "migration",
    },
    {
      key: 1004054,
      title: "mikro-orm",
    },
    {
      key: 1004070,
      title: "swagger",
    },
    {
      key: 1004080,
      title: "config",
    },
    {
      key: 1004090,
      title: "redis",
    },
    {
      key: 1004110,
      title: "Docker",
    }
  ]);
});
