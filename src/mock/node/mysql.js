import Mock from 'mockjs'
export default Mock.mock('/node/mysql', 'get', options => {
  return Mock.mock([
    {
      key: 1010000,
      title: "windows安装",
    },
    {
      key: 1010001,
      title: "可视化管理",
    },
    {
      key: 1010009,
      title: "命令大小写",
    },
    {
      key: 1010010,
      title: "数据库命令",
    },
    {
      key: 1010100,
      title: "sql",
    },
    {
      key: 1010101,
      title: "sql2",
    }
  ]);
})