import Mock from 'mockjs'
export default Mock.mock('/node/basic', 'get', options => {
  return Mock.mock([
    {
      key: 1001000,
      title: "介绍",
    },
    {
      key: 1001001,
      title: "windows安装",
    },
    {
      key: 1001007,
      title: "版本切换",
    },
    {
      key: 1001011,
      title: "运行js",
    },
    {
      key: 1001021,
      title: "模块化",
    },
    {
      key: 1001031,
      title: "fs",
    },
    {
      key: 1001032,
      title: "路径问题",
    },
    {
      key: 1001041,
      title: "path",
    },
    {
      key: 1001051,
      title: "http",
    },
    {
      key: 1001052,
      title: "差别响应",
    },
    {
      key: 1001061,
      title: "querystring",
    },

  ]);
})