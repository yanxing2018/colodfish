import Mock from "mockjs";
export default Mock.mock("/miniPro/basic", "get", (options) => {
  return Mock.mock([
    {
      key: 1101000,
      title: "开发环境",
    },
    {
      key: 1101001,
      title: "vscode",
    },
    {
      key: 1101010,
      title: "创建项目",
    },
    {
      key: 1101050,
      title: "自定义组件",
    },
    {
      key: 1101060,
      title: "custom-tab-bar",
    },
    {
      key: 1101070,
      title: "iconfont单色",
    },
    {
      key: 1101071,
      title: "iconfont彩色",
    },
    {
      key: 1101080,
      title: "日期时间",
    },
    {
      key: 1101100,
      title: "tdesign",
    },
    {
      key: 1101200,
      title: "高度计算",
    },
  ]);
});
