import Mock from "mockjs";
export default Mock.mock("/miniPro/cloud", "get", (options) => {
  return Mock.mock([
    {
      key: 1102000,
      title: "创建项目",
    },
    {
      key: 1102010,
      title: "收费",
    },
    {
      key: 1102020,
      title: "初始化配置",
    },
    {
      key: 1102030,
      title: "新建云函数",
    },
    {
      key: 1102031,
      title: "云函数参数",
    }
  ]);
});