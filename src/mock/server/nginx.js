import Mock from 'mockjs'
export default Mock.mock('/server/nginx', 'get', options => {
  return Mock.mock([
    {
      key: 1201000,
      title: "环境配置",
    },
    {
      key: 1201001,
      title: "跨域",
    },
    {
      key: 1201002,
      title: "刷新404",
    },
    {
      key: 1201012,
      title: "停止/结束",
    }
  ]);
})