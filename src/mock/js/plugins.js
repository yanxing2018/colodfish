import Mock from 'mockjs'
export default Mock.mock('/js/plugins', 'get', options=>{
    return Mock.mock([
        {
            key: 411000,
            title:"mocha",
        },
        {
            key: 411001,
            title:"math",
        },
        {
            key: 411002,
            title:"lodash",
        },
        {
            key: 411103,
            title:"video",
        },
        {
            key: 411104,
            title:"Hls.js",
        },
        {
            key: 411200,
            title:"js-md5",
        },
        {
            key: 411220,
            title:"rc-menu",
        },
    ]);
})