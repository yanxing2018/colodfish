import Mock from 'mockjs'
export default Mock.mock('/js/async', 'get', options => {
    return Mock.mock([
        {
            key: 407000,
            title: "event_loop",
        },
        {
            key: 407001,
            title: "异步任务",
        },
        {
            key: 407002,
            title: "浏览器线程",
        },
        {
            key: 407010,
            title: "iterator",
        },
        {
            key: 407011,
            title: "generate",
        },
        {
            key: 407012,
            title: "async/await",
        },
        {
            key: 407021,
            title: "Promise",
        },
    ]);
})