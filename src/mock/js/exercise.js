import Mock from 'mockjs'
export default Mock.mock('/js/exercise', 'get', options => {
  return Mock.mock([
    {
      key: 410000,
      title: "小数运算",
    },
    {
      key: 410001,
      title: "回文字符串",
    },
    {
      key: 410002,
      title: "格式化数字",
    },
    {
      key: 410003,
      title: "数组扁平化",
    },
    {
      key: 410004,
      title: "获取元素",
    },
    {
      key: 410010,
      title: "闭包",
    },
    {
      key: 410006,
      title: "变量输出",
    },
    {
      key: 410007,
      title: "大数相加",
    },
    {
      key: 410008,
      title: "数组去重",
    },
    {
      key: 410009,
      title: "DOM树",
    },
  ]);
})