import Mock from 'mockjs'
export default Mock.mock('/js/date', 'get', options=>{
    return Mock.mock([
        {
            key: 406000,
            title:"date对象",
        },
        {
            key: 406011,
            title:"moment",
        },
        {
            key: 406012,
            title:"moment对象",
        },
        {
            key: 406013,
            title:"时间转换",
        },
        {
            key: 406014,
            title:"格式化",
        },
        {
            key: 406020,
            title:"0点和24点",
        },
        {
            key: 406021,
            title:"是否今天",
        },
    ]);
})