import Mock from 'mockjs'
export default Mock.mock('/js/bom', 'get', options=>{
    return Mock.mock([
        {
            key: 403000,
            title:"基础概念",
        },
        {
            key: 403001,
            title:"window对象",
        },
        {
            key: 403004,
            title:"document对象",
        },
        {
            key: 403005,
            title:"history对象",
        },
        {
            key: 403006,
            title:"location对象",
        },
        {
            key: 403007,
            title:"Navigator对象",
        },
        {
            key: 403110,
            title:"onerror",
        },
    ]);
})