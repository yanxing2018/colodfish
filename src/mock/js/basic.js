
import Mock from 'mockjs'
export default Mock.mock('/js/basic', 'get', options=>{
    return Mock.mock([
      {
        key: 401000,
        title:"变量定义",
      },
      {
        key: 401001,
        title:"变量类型",
      },
      {
        key: 401002,
        title:"函数",
      },
      {
        key: 401003,
        title:"运算",
      },
      {
        key: 401004,
        title:"对象",
      },
      {
        key: 401005,
        title:"继承",
      },
      {
        key: 401006,
        title:"原型链",
      },
      {
        key: 401007,
        title:"异常",
      },
      {
        key: 401008,
        title:"闭包",
      },
      {
        key: 401009,
        title:"判断类型",
      },
      {
        key: 401015,
        title:"Math",
      },
      {
        key: 401017,
        title:"null和undefined",
      },
      {
        key: 401018,
        title:"parseInt",
      },
      {
        key: 401019,
        title:"模块化",
      },
      {
        key: 401020,
        title:"数组遍历",
      },
      {
        key: 401021,
        title:"forEach",
      },
      {
        key: 401022,
        title:"map",
      },
      {
        key: 401023,
        title:"filter",
      },
      {
        key: 401024,
        title:"reduce",
      },
      {
        key: 401025,
        title:"sort",
      },
      {
        key: 401030,
        title:"slice",
      },
      {
        key: 401031,
        title:"splice",
      },
    ]);
})