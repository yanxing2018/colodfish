import Mock from 'mockjs'
export default Mock.mock('/js/regexp', 'get', options=>{
    return Mock.mock([
        {
            key: 405000,
            title:"语法",
        },
        {
            key: 405001,
            title:"字符",
        },
        {
            key: 405002,
            title:"标志位g",
        },
        {
            key: 405003,
            title:"exec",
        },
        {
            key: 405004,
            title:"replace",
        },
        {
            key: 405101,
            title:"截取中文字符",
        },
    ]);
})