import Mock from 'mockjs'
export default Mock.mock('/react/hooks', 'get', options=>{
    return Mock.mock([
        {
            key: 603000,
            title:"hooks",
        },
        {
            key: 603001,
            title:"useState",
        },
        {
            key: 603002,
            title:"useReducer",
        },
        {
            key: 603003,
            title:"useEffect",
        },
        {
            key: 603004,
            title:"useContext",
        },
        {
            key: 603005,
            title:"useRef",
        },
        {
            key: 603006,
            title:"useCallback",
        },
        {
            key: 603007,
            title:"useMemo",
        },
        {
            key: 603008,
            title:"useLayoutEffect",
        },
        {
            key: 603009,
            title:"useImperativeHandle",
        },
        {
            key: 603020,
            title:"自定义",
        },
        {
            key: 603021,
            title:"多个state",
        },
        {
            key: 603022,
            title:"批量更新",
        },
        {
            key: 603030,
            title:"hooks闭包",
        },
        {
            key: 603031,
            title:"闭包陷阱",
        },
    ]);
})