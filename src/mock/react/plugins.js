import Mock from 'mockjs'
export default Mock.mock('/react/plugins', 'get', options=>{
    return Mock.mock([
        {
            key: 607000,
            title:"拖拽",
        },
        {
            key: 607001,
            title:"环境部署",
        },
        {
            key: 607002,
            title:"craco",
        },
        {
            key: 607003,
            title:"webpack",
        },
        {
            key: 607040,
            title:"rc-util",
        },
        {
            key: 607100,
            title:"swagger-ui-react",
        },
        {
            key: 607101,
            title:"classnames",
        },
        {
            key: 607102,
            title:"react-apng",
        },
        {
            key: 607103,
            title:"react-apng",
        },
        {
            key: 607104,
            title:"react-dev-inspector",
        },
        {
            key: 607105,
            title:"react-helmet-async",
        },
        {
            key: 607106,
            title:"react-keyevent",
        },
        {
            key: 607107,
            title:"react-seamless-scroll",
        },
        {
            key: 607200,
            title:"@jiaminghi/data-view-react",
        }
    ]);
})