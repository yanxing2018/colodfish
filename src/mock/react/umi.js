import Mock from 'mockjs'
export default Mock.mock('/react/umi', 'get', options=>{
    return Mock.mock([
        {
            key: 609000,
            title:"基础介绍",
        },
        {
            key: 609013,
            title:"路由",
        },
        {
            key: 609014,
            title:"权限",
        },
        {
            key: 609030,
            title:"dva",
        },
        {
            key: 609040,
            title:"umi-request",
        },
        {
            key: 609041,
            title:"umi-serve",
        },
    ]);
})