import Mock from 'mockjs'
export default Mock.mock('/react/router', 'get', options=>{
    return Mock.mock([
        {
            key: 604000,
            title:"安装部署",
        },
        {
            key: 604001,
            title:"项目使用",
        },
        {
            key: 604002,
            title:"守卫",
        },
        {
            key: 604003,
            title:"跳转",
        },
        {
            key: 604004,
            title:"传参",
        },
        {
            key: 604005,
            title:"NavLink",
        },
        {
            key: 604006,
            title:"动态路由",
        },
        {
            key: 604007,
            title:"嵌套路由",
        },
        {
            key: 604008,
            title:"重定向",
        },
        {
            key: 604009,
            title:"5和6区别",
        },
        {
            key: 604010,
            title:"懒加载",
        },
    ]);
})