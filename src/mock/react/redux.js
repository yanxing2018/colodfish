import Mock from 'mockjs'
export default Mock.mock('/react/redux', 'get', options=>{
    return Mock.mock([
        {
            key: 605000,
            title:"基础",
        },
        {
            key: 605001,
            title:"状态管理",
        },
        {
            key: 605011,
            title:"Hooks状态",
        }
    ]);
})