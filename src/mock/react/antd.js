import Mock from 'mockjs'
export default Mock.mock('/react/antd', 'get', options => {
    return Mock.mock([
        {
            key: 606000,
            title: "按需引入",
        },
        {
            key: 606001,
            title: "自定义主题",
        },
        {
            key: 606002,
            title: "日期选择",
        },
        {
            key: 606010,
            title: "form基础",
        },
        {
            key: 606011,
            title: "autoComplete",
        },
        {
            key: 606012,
            title: "useForm",
        },
        {
            key: 606013,
            title: "ProFormList",
        },
        {
            key: 606030,
            title: "table基础",
        },
        {
            key: 606031,
            title: "ProTable",
        },
        {
            key: 606032,
            title: "table样式",
        },
        {
            key: 606033,
            title: "滚动组件封装",
        },
        {
            key: 606108,
            title: "Carousel",
        },
        {
            key: 606109,
            title: "数字跳转",
        },
        {
            key: 606150,
            title: "文件上传/下载",
        },
    ]);
})