import Mock from 'mockjs'
export default Mock.mock('/react/practice', 'get', options=>{
    return Mock.mock([
        {
            key: 608000,
            title:"项目初始化",
        },
        {
            key: 608001,
            title:"组件外变量",
        },
        {
            key: 608002,
            title:"渲染优化",
        },
        {
            key: 608020,
            title:"组件初始",
        },
    ]);
})