import Mock from 'mockjs'
export default Mock.mock('/visual/mars3d', 'get', options => {
  return Mock.mock([
    {
      key: 902000,
      title: "基础介绍",
    },
    {
      key: 902001,
      title: "react使用",
    },
    {
      key: 902007,
      title: "视角",
    },
    {
      key: 902008,
      title: "坐标系转换",
    },
    {
      key: 902009,
      title: "图层",
    },
    {
      key: 902010,
      title: "矢量图层",
    },
    {
      key: 902011,
      title: "文本点",
    },
    {
      key: 902012,
      title: "像素点",
    },
    {
      key: 902013,
      title: "图标点",
    },
    {
      key: 902014,
      title: "平面",
    },
    {
      key: 902015,
      title: "线",
    },
    {
      key: 902016,
      title: "模型",
    },
    {
      key: 902017,
      title: "盒子",
    },
    {
      key: 902018,
      title: "圆圆柱",
    },
    {
      key: 902019,
      title: "椭圆",
    },
    {
      key: 902020,
      title: "圆锥",
    },
    {
      key: 902021,
      title: "球体",
    },
    {
      key: 902022,
      title: "管道",
    },
    {
      key: 902023,
      title: "路径",
    },
    {
      key: 902024,
      title: "走廊",
    },
    {
      key: 902025,
      title: "墙",
    },
    {
      key: 902026,
      title: "面",
    },
    {
      key: 902027,
      title: "矩形",
    },
    {
      key: 902031,
      title: "聚合",
    },
    {
      key: 902032,
      title: "popup",
    },
    {
      key: 902060,
      title: "thing",
    },
    {
      key: 902061,
      title: "旋转",
    },
    {
      key: 902100,
      title: "判断经纬度",
    },
    {
      key: 902110,
      title: "波纹扩散",
    },
  ]);
})