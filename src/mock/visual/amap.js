import Mock from 'mockjs'
export default Mock.mock('/visual/amap', 'get', options=>{
    return Mock.mock([
          {
            key: 903000,
            title:"高德地图",
          },
          {
            key: 903001,
            title:"JS API",
          },
          {
            key: 903010,
            title:"自定义样式",
          },
          {
            key: 903011,
            title:"图层",
          },
          {
            key: 903101,
            title:"地图不显示",
          },
          {
            key: 903201,
            title:"点标记",
          },
          {
            key: 903202,
            title:"轨迹回放",
          },
          {
            key: 903211,
            title:"信息窗体",
          },
          {
            key: 903221,
            title:"获取覆盖物",
          },
    ]);
})