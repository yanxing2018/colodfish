import Mock from 'mockjs'
export default Mock.mock('/visual/echarts', 'get', options=>{
    return Mock.mock([
          {
            key: 901000,
            title:"快速上手",
          },
          {
            key: 901001,
            title:"vue安装",
          },
          {
            key: 901002,
            title:"react安装",
          },
          {
            key: 901003,
            title:"图表不显示",
          },
          {
            key: 901004,
            title:"主题",
          },
          {
            key: 901005,
            title:"颜色",
          },
          {
            key: 901006,
            title:"样式",
          },
          {
            key: 901007,
            title:"自适应",
          },
          {
            key: 901008,
            title:"动画",
          },
          {
            key: 901009,
            title:"交互",
          },
          {
            key: 901010,
            title:"配置项",
          },
          {
            key: 901011,
            title:"折线图",
          },
          {
            key: 901012,
            title:"散点图",
          },
          {
            key: 901013,
            title:"直角坐标系",
          },
          {
            key: 901014,
            title:"饼图",
          },
          {
            key: 901015,
            title:"地图",
          },
          {
            key: 901016,
            title:"雷达图",
          },
          {
            key: 901017,
            title:"仪表盘",
          },
          {
            key: 901018,
            title:"屏幕适配",
          },
          {
            key: 901019,
            title:"按需引入",
          },
          {
            key: 901099,
            title:"问题",
          },
    ]);
})