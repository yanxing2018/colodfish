import Mock from 'mockjs'
export default Mock.mock('/visual/plugins', 'get', options=>{
    return Mock.mock([
        {
            key: 911000,
            title:"turf",
        },
        {
            key: 911100,
            title:"echarts-gl",
        },
    ]);
})