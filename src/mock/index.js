// import Mock from 'mockjs'
// export default Mock.mock('/w3c/html', 'get', options=>{
//     return Mock.mock([
//     ]);
// })

// caprice
import cproject from "./caprice/project";
import cprinciple from "./caprice/principle";

// html
import hbasic from "./html/basic";

// css
import cbasic from "./css/basic";
import cless from "./css/less";
import csass from "./css/sass";
import ccolor from "./css/color";
import cpractice from "./css/practice";

// js
import jbasic from "./js/basic";
import jbom from "./js/bom";
import jdom from "./js/dom";
import ajax from "./js/ajax";
import jexercise from "./js/exercise";
import jplugins from "./js/plugins";
import jregexp from "./js/regexp";
import jdate from "./js/date";
import jasync from "./js/async";

// login
import login from "./login/login";
import token from "./login/token";

// vue
import vbasic from "./vue/basic";
import vcli from "./vue/cli";
import vdirective from "./vue/directives";
import vrouter from "./vue/router";
import vvuex from "./vue/vuex";
import vpractice from "./vue/practice";
import velement from "./vue/element";
import vplugins from "./vue/plugins";

// react
import rbasic from "./react/basic";
import rantd from "./react/antd";
import rcra from "./react/cra";
import rrouter from "./react/router";
import rhooks from "./react/hooks";
import rplugins from "./react/plugins";
import redux from "./react/redux";
import rpractice from "./react/practice";
import rumi from "./react/umi";
import rnext from "./react/next";

// tool
import git from "./tool/git";
import mock from "./tool/mock";
import vscode from "./tool/vscode";
import computer from "./tool/computer";
import webpack from "./tool/webpack";
import eslint from "./tool/eslint";

// topic
import mobile from "./topic/mobile";
import compatible from "./topic/compatible";
import optimize from "./topic/optimize";
import safety from "./topic/safety";

// visualization
import echarts from "./visual/echarts";
import mars3d from "./visual/mars3d";
import amap from "./visual/amap";
import vsplugins from "./visual/plugins";

// node
import nbasic from "./node/basic";
import nexpress from "./node/express";
import nkoa from "./node/koa";
import nnest from "./node/nest";
import negg from "./node/egg";
import nmidway from "./node/midway";
import nmysql from "./node/mysql";

// 小程序
import mbasic from "./miniPro/basic";
import mcloud from "./miniPro/cloud";
import mproblem from "./miniPro/problem";

// 服务器
import nginx from "./server/nginx";

export {
  cproject, cprinciple,
  hbasic,
  cbasic, cless, cpractice, ccolor, csass,
  jbasic, ajax, jbom, jexercise, jplugins, jdom, jregexp, jdate, jasync,
  login, token,
  vbasic, vcli, vdirective, vrouter, vvuex, vpractice, velement, vplugins,
  rbasic, rantd, rcra, rrouter, rhooks, rplugins, redux, rpractice, rumi, rnext,
  nginx, mobile,
  compatible, optimize, safety,
  mock, computer, vscode, webpack, eslint, git,
  echarts, mars3d, amap, vsplugins,
  nbasic, nexpress, nkoa, nnest, negg, nmidway, nmysql,
  mbasic, mcloud, mproblem
};
