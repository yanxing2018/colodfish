import Mock from 'mockjs'
export default Mock.mock('/css/color', 'get', options => {
    return Mock.mock([
        {
            key: 306000,
            title: "颜色基础",
        },
        {
            key: 306001,
            title: "vue",
        },
        {
            key: 306010,
            title: "杭州地铁",
        },
        {
            key: 306040,
            title: "颜色转换",
        },
        {
            key: 306050,
            title: "传统色",
        },
        {
            key: 306150,
            title: "button",
        },
    ]);
})