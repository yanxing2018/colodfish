import Mock from 'mockjs'
export default Mock.mock('/css/practice', 'get', options=>{
    return Mock.mock([
        {
            key: 310000,
            title:"四角框",
        },
        {
            key: 310031,
            title:"文本对齐",
        },
        {
            key: 310032,
            title:"div蒙层",
        },
    ]);
})