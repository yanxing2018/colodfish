// 左侧导航栏

import { ReactNode } from "react";
import Iconfont from "assets/font/iconfont";

export interface IMenuItem {
  title: string;
  key: string;
  icon: ReactNode;
  role?: string[];
  children?: IMenuItem[];
}
const menuList: IMenuItem[] = [
  {
    title: "主页",
    key: "/",
    icon: <Iconfont type="icon-home" />,
  },
  {
    title: "关于",
    key: "caprice",
    icon: <Iconfont type="icon-icon-test" />,
    role: ["caprice"],
    children: [
      {
        title: "关于项目",
        key: "caprice/project",
        icon: <Iconfont type="icon-xiangmu" />,
      },
      {
        title: "基本原则",
        key: "caprice/principle",
        icon: <Iconfont type="icon-shezhijizhun" />,
      },
    ],
  },
  {
    title: "html",
    key: "html",
    icon: <Iconfont type="icon-h5e" />,
    role: ["html"],
    children: [
      {
        title: "basic",
        key: "html/basic",
        icon: <Iconfont type="icon-neirong2" />,
      },
    ],
  },
  {
    title: "css",
    key: "css",
    icon: <Iconfont type="icon-css3" />,
    role: ["css"],
    children: [
      {
        title: "basic", // 1
        key: "css/basic",
        icon: <Iconfont type="icon-neirong2" />,
      },
      {
        title: "变形动画", // 2
        key: "css/animation",
        icon: <Iconfont type="icon-donghuapian" />,
      },
      {
        title: "特效", // 3
        key: "css/effects",
        icon: <Iconfont type="icon-texiao" />,
      },
      {
        title: "less", // 4
        key: "css/less",
        icon: <Iconfont type="icon-less" />,
      },
      {
        title: "sass", // 5
        key: "css/sass",
        icon: <Iconfont type="icon-sass" />,
      },
      {
        title: "color", // 6
        key: "css/color",
        icon: <Iconfont type="icon-yanse" />,
      },
      {
        title: "practice", // 10
        key: "css/practice",
        icon: <Iconfont type="icon-lianxi" />,
      },
    ],
  },
  {
    title: "js",
    key: "js",
    icon: <Iconfont type="icon-js" />,
    role: ["js"],
    children: [
      {
        title: "basic", // 1
        key: "js/basic",
        icon: <Iconfont type="icon-neirong2" />,
      },
      {
        title: "dom", // 2
        key: "js/dom",
        icon: <Iconfont type="icon-dom" />,
      },
      {
        title: "bom", // 3
        key: "js/bom",
        icon: <Iconfont type="icon-BOM" />,
      },
      {
        title: "ajax", // 4
        key: "js/ajax",
        icon: <Iconfont type="icon-aJax" />,
      },
      {
        title: "regexp", // 5
        key: "js/regexp",
        icon: <Iconfont type="icon-canvas_text" />,
      },
      {
        title: "date", // 6
        key: "js/date",
        icon: <Iconfont type="icon-shijian" />,
      },
      {
        title: "async", // 7
        key: "js/async",
        icon: <Iconfont type="icon-angle-compromise" />,
      },
      {
        title: "exercise", // 10
        key: "js/exercise",
        icon: <Iconfont type="icon-lianxi" />,
      },
      {
        title: "plugins", // 11
        key: "js/plugins",
        icon: <Iconfont type="icon-chajian1" />,
      },
    ],
  },
  // vue 5
  {
    title: "vue",
    key: "vue",
    icon: <Iconfont type="icon-bxl-vuejs" />,
    role: ["vue"],
    children: [
      {
        title: "basic",
        key: "vue/basic",
        icon: <Iconfont type="icon-neirong2" />,
      },
      {
        title: "cli",
        key: "vue/cli",
        icon: <Iconfont type="icon-jiaoshoujia-xian" />,
      },
      {
        title: "directives",
        key: "vue/directives",
        icon: <Iconfont type="icon-zhilingguanli" />,
      },
      {
        title: "router",
        key: "vue/router",
        icon: <Iconfont type="icon-luyou" />,
      },
      {
        title: "vuex",
        key: "vue/vuex",
        icon: <Iconfont type="icon-ecs-status" />,
      },
      {
        title: "element",
        key: "vue/element",
        icon: <Iconfont type="icon-neirong2" />,
      },
      {
        title: "plugins",
        key: "vue/plugins",
        icon: <Iconfont type="icon-chajian1" />,
      },
      {
        title: "practice",
        key: "vue/practice",
        icon: <Iconfont type="icon-lianxi" />,
      },
    ],
  },
  // react 6
  {
    title: "react",
    key: "react",
    icon: <Iconfont type="icon-reactjs-line" />,
    role: ["react"],
    children: [
      {
        title: "basic", //1
        key: "react/basic",
        icon: <Iconfont type="icon-neirong2" />,
      },
      {
        title: "cra", //2
        key: "react/cra",
        icon: <Iconfont type="icon-jiaoshoujia-xian" />,
      },
      {
        title: "hooks", //3
        key: "react/hooks",
        icon: <Iconfont type="icon-Crain-Hook" />,
      },
      {
        title: "router", //4
        key: "react/router",
        icon: <Iconfont type="icon-luyou" />,
      },
      {
        title: "redux", //5
        key: "react/redux",
        icon: <Iconfont type="icon-ecs-status" />,
      },
      {
        title: "antd", //6
        key: "react/antd",
        icon: <Iconfont type="icon-antdesign" />,
      },
      {
        title: "plugins", //7
        key: "react/plugins",
        icon: <Iconfont type="icon-chajian1" />,
      },
      {
        title: "practice", //8
        key: "react/practice",
        icon: <Iconfont type="icon-lianxi" />,
      },
      {
        title: "umi", //9
        key: "react/umi",
        icon: <Iconfont type="icon-jinkoumimianyou" />,
      },
      {
        title: "next", //10
        key: "react/next",
        icon: <Iconfont type="icon-next-dot-js" />,
      },
    ],
  },
  {
    title: "话题",
    key: "topic",
    icon: <Iconfont type="icon-topic" />,
    role: ["topic"],
    children: [
      {
        title: "移动端", // 2
        key: "topic/mobile",
        icon: <Iconfont type="icon-xiangyingshi" />,
      },
      {
        title: "优化", // 3
        key: "topic/optimize",
        icon: <Iconfont type="icon-optimization" />,
      },
      {
        title: "安全", // 4
        key: "topic/safety",
        icon: <Iconfont type="icon-anquan" />,
      },
      {
        title: "兼容性", // 5
        key: "topic/compatible",
        icon: <Iconfont type="icon-jiyukaiyuanjianrongkaiyuan" />,
      },
    ],
  },
  {
    title: "工具",
    key: "tool",
    icon: <Iconfont type="icon-gongju" />,
    role: ["tool"],
    children: [
      {
        title: "git", // 1
        key: "tool/git",
        icon: <Iconfont type="icon-git" />,
      },
      {
        title: "iconfont", // 2
        key: "tool/iconfont",
        icon: <Iconfont type="icon-Iconfont" />,
      },
      {
        title: "mock", // 3
        key: "tool/mock",
        icon: <Iconfont type="icon-MockTest" />,
      },
      {
        title: "vscode", // 4
        key: "tool/vscode",
        icon: <Iconfont type="icon-VsCode" />,
      },
      {
        title: "computer", // 5
        key: "tool/computer",
        icon: <Iconfont type="icon-huanjingsheji" />,
      },
      {
        title: "webpack", // 6
        key: "tool/webpack",
        icon: <Iconfont type="icon-webpack" />,
      },
      {
        title: "eslint", // 7
        key: "tool/eslint",
        icon: <Iconfont type="icon-eslint" />,
      },
    ],
  },
  {
    title: "可视化",
    key: "visual",
    icon: <Iconfont type="icon-keshihuatubiao-" />,
    role: ["visual"],
    children: [
      {
        title: "echarts", // 1
        key: "visual/echarts",
        icon: <Iconfont type="icon-echarts_cross" />,
      },
      {
        title: "mars3d", // 2
        key: "visual/mars3d",
        icon: <Iconfont type="icon-marshall-island" />,
      },
      {
        title: "高德", // 3
        key: "visual/amap",
        icon: <Iconfont type="icon-mapOfGaud" />,
      },
      {
        title: "plugins", // 11
        key: "visual/plugins",
        icon: <Iconfont type="icon-chajian1" />,
      },
    ],
  },
  // node 10
  {
    title: "node",
    key: "node",
    icon: <Iconfont type="icon-Nodejs" />,
    role: ["node"],
    children: [
      {
        title: "basic", //1
        key: "node/basic",
        icon: <Iconfont type="icon-neirong2" />,
      },
      {
        title: "express", //2
        key: "node/express",
        icon: <Iconfont type="icon-express" />,
      },
      {
        title: "koa", //3
        key: "node/koa",
        icon: <Iconfont type="icon-koala" />,
      },
      {
        title: "nest", //4
        key: "node/nest",
        icon: <Iconfont type="icon-nest-service" />,
      },
      {
        title: "egg", //5
        key: "node/egg",
        icon: <Iconfont type="icon-Egg" />,
      },
      {
        title: "midway", //6
        key: "node/midway",
        icon: <Iconfont type="icon-pyramid" />,
      },
      {
        title: "mysql", //10
        key: "node/mysql",
        icon: <Iconfont type="icon-mysql" />,
      },
    ],
  },
  // 小程序-11
  {
    title: "小程序",
    key: "miniPro",
    icon: <Iconfont type="icon-xiaochengxu" />,
    role: ["miniPro"],
    children: [
      {
        title: "basic", //1
        key: "miniPro/basic",
        icon: <Iconfont type="icon-neirong2" />,
      },
      {
        title: "cloud", //2
        key: "miniPro/cloud",
        icon: <Iconfont type="icon-cloud" />,
      },
      {
        title: "problem", //10
        key: "miniPro/problem",
        icon: <Iconfont type="icon-wenti" />,
      },
    ],
  },
  // 服务器-12
  {
    title: "服务器",
    key: "server",
    icon: <Iconfont type="icon-fuwuqi" />,
    role: ["server"],
    children: [
      {
        title: "nginx", //1
        key: "server/nginx",
        icon: <Iconfont type="icon-nginx" />,
      }
    ],
  },
];

export default menuList;
